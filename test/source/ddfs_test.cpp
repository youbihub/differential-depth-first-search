#include <ddfs/version.h>
#include <doctest/doctest.h>

#include <string>

TEST_CASE("Ddfs version") {
  static_assert(std::string_view(DDFS_VERSION) == std::string_view("1.0"));
  CHECK(std::string(DDFS_VERSION) == std::string("1.0"));
}
